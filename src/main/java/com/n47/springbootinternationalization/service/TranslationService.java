package com.n47.springbootinternationalization.service;

import org.springframework.stereotype.Service;

import static com.n47.springbootinternationalization.config.Translator.toLocale;
import static com.n47.springbootinternationalization.util.TranslatorCode.GREETINGS;

@Service
public class TranslationService {

    public String translate() {
        return toLocale(GREETINGS);
    }
}
